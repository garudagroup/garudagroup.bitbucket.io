//select element function

const selectElement = function(element){
    return document.querySelector(element);
};
// control the toogle menu from mobile view
let menuToggler = selectElement('.menu-toggle');
let body = selectElement('body');

menuToggler.addEventListener('click', function(){
    body.classList.toggle('open');
});

//Scrool reveal = resources https://scrollrevealjs.org/

window.sr = ScrollReveal(); // function for the animation

// sr = scrool. if the function are called in index.html the animation will reveal within the element

sr.reveal('.animate-left',{
    origin: 'left', // position from the element reveal
    duration: 1000, // the time to complete the animation (smooth effect)
    distance: '25rem', // control the range of each element when it reveal
    delay: 300 // the deleay before animation reveal (smooth effect)
});

sr.reveal('.animate-right',{
    origin: 'right',
    duration: 1000,
    distance: '25rem',
    delay: 600
});

sr.reveal('.animate-top',{
    origin: 'top',
    duration: 1000,
    distance: '25rem',
    delay: 600
});

sr.reveal('.animate-bottom',{
    origin: 'bottom',
    duration: 1000,
    distance: '25rem',
    delay: 600
});

// pop up form section

// this funtion control the form when sign in is on click open the form by adding block so it apaer to the user
function openForm() {
  document.getElementById("myForm").style.display = "block";
}

// this fucntion close the form when use click close button
function closeForm() {
  document.getElementById("myForm").style.display = "none";
}